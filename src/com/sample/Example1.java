package com.sample;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Taewoo, Kim(Michael) (5zzang@gmail.com)
 * @since 09-09-2015
 *
 * Print "1 : 2 : 3 : 4 : 5 : 6 : 7 : 8 : 9 : 10"
 */
public class Example1 {
    public static void main(String[] args) {
        // old style
        final List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
//        final StringBuilder sb = new StringBuilder();
//
//        final int size = numbers.size();
//        for (int i = 0; i < size; i++) {
//            sb.append(numbers.get(i));
//
//            if (i != size-1) {
//                sb.append(" : ");
//            }
//        }
//
//        System.out.println(sb.toString());

        // new style
        final String result = numbers.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(":"));
        System.out.println(result);
    }
}
